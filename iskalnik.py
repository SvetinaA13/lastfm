__author__ = 'megasveco'
from tkinter import *
from tkinter import ttk
import threading
from razred import *


api = ''

class Iskalnik:
    def __init__(self, master):
        self.username = StringVar()
        #self.datum_izdaje_var = StringVar()
        self.zanri_var = StringVar()
        self.pesmi = StringVar()
        self.trajanje = StringVar()
        self.seznamAlbumov = StringVar()

# ################     Okvir     #################################################################

        self.frame = Frame(master, width=400, height=300)
        self.frame.grid(column=0, row=0, columnspan=6, rowspan=6)

        vnos = Frame(self.frame, width=400, height=40)  # left, top, right, bottom
        vnos.grid(column=0, row=0, columnspan=2)

        # ###############     Gumbi, vnosna polja     ###################################################

        name = Entry(vnos, textvariable=self.username)
        name.grid(column=0, row=0, padx=(20, 70), pady=40)

        button = Button(vnos, text='Najdi', command=self.poisci)
        button.grid(column=1, row=0, padx=10, pady=30)

    def poisci(self):
        self.ogrodje()
        self.l1.delete(0, END)
        self.user = User(user=self.username.get(), okno=self)
        thread = threading.Thread(target=self.user.poisci)
        thread.daemon = True
        thread.start()

    def ogrodje(self):
        albumi_fr = Frame(self.frame, width=400, height=200, borderwidth=2, relief='sunken')
        albumi_fr.grid(column=0, row=1, columnspan=3, rowspan=5, sticky=(N, S, E, W))

        albumi_lab = ttk.Label(albumi_fr, text='Največkrat poslušani albumi:', font='TkCaptionFont')
        albumi_lab.grid(column=0, row=0, columnspan=2, sticky=(N, S, E, W))

        self.prazno = Frame(albumi_fr, width=200, height=150, borderwidth=2)
        self.prazno.grid(column=0, row=1, columnspan=3, rowspan=4, sticky=(N, S, E, W))

        self.podatki = Frame(self.frame, width=200, height=150, borderwidth=10, relief='raised')
        self.podatki.grid(column=4, row=1, columnspan=3, rowspan=5, sticky=(N, S))

        pesmi = Label(self.podatki, width=30, height=1, text='Pesmi:', font='TkCaptionFont', justify=LEFT, anchor=W)
        pesmi.grid(column=0, row=3, columnspan=2, sticky='E')

        self.prazno2 = Frame(self.podatki, borderwidth=2)
        self.prazno2['width'] = 200
        self.prazno2['height'] = 100
        self.prazno2.grid(column=0, row=4, columnspan=3, sticky=(E, W))


# ###############     Tekst        ######################################

        self.zanri = Label(self.podatki, width=23, height=4, textvariable=self.zanri_var, justify=LEFT, anchor=NW)
        self.zanri['font'] = ('Arial', 12)
        self.zanri['wraplength'] = 230
        self.zanri.grid(column=0, row=1, sticky=(E, W))

        self.l1 = Listbox(self.prazno, listvariable=self.seznamAlbumov, height=20, width=40)
        self.l1.grid(column=0, row=0, columnspan=2, rowspan=4, sticky=(E, W))

        s1 = ttk.Scrollbar(self.prazno, orient=VERTICAL, command=self.l1.yview)
        s1.grid(column=2, row=0, columnspan=1, rowspan=4, sticky=(N, S, E, W))

        self.l1.configure(yscrollcommand=s1.set)
        self.l1.bind('<<ListboxSelect>>', self.izbira)

        self.list_pesmi = Listbox(self.prazno2, listvariable=self.pesmi, height=10, width=34)
        self.list_pesmi.grid(column=0, row=0, columnspan=1, sticky=(E, W))

        self.list_trajanje = Listbox(self.prazno2, listvariable=self.trajanje, height=10, width=5)
        self.list_trajanje.grid(column=1, row=0, columnspan=1)

        s2 = Scrollbar(self.prazno2, orient=VERTICAL, command=self.scroll)
        s2.grid(column=2, row=0, sticky=(N, S))

        self.list_pesmi.configure(yscrollcommand=s2.set)
        self.list_trajanje.configure(yscrollcommand=s2.set)


    def izbira(self, event):
        indeks = int(self.l1.curselection()[0])
        self.album = self.user.seznam_albumov[indeks]

        self.pesmi.set(tuple(self.album.pesmi))
        self.trajanje.set(tuple(self.album.trajanje))
        self.zanri_var.set('Oznake: ' + self.album.tags)

    def scroll(self, *args):
        self.list_pesmi.yview(*args)
        self.list_trajanje.yview(*args)


root = Tk()
root.title('Iskalnik')
iskalnik = Iskalnik(root)


root.mainloop()