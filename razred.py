__author__ = 'SvetinaA13'

import re
import requests
from bs4 import BeautifulSoup

api = ''
slo_meseci = ['januar', 'februar', 'marec', 'april', 'maj', 'junij',
              'julij', 'avgust', 'september', 'oktober', 'november', 'december']
ang_meseci = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


class User():
    def __init__(self, user='', stalbumov=10, okno=''):
        self.username = user
        self.st_albumov = stalbumov
        self.seznam_albumov = []
        self.okno = okno

    def poisci(self):
        stran = 1
        g = []
        h = []
        stikalo = 0

        while len(g) < self.st_albumov:
            url = ('http://ws.audioscrobbler.com/2.0/?method=user.gettopalbums&user='
                   + self.username
                   + '&page='
                   + str(stran)
                   + '&api_key='
                   + api)

            response = requests.get(url)

            soup = BeautifulSoup(response.content)
            f = []
            l = []

            a = soup.find_all('album')
            for i in a:
                f += [re.sub(re.escape('</name>')+r'.*', '', str(i), flags=re.DOTALL)]
            for i in f:
                if [re.sub('(.*?)'+re.escape('<name>'), '', str(i), flags=re.DOTALL)] not in g:
                    g += [re.sub('(.*?)'+re.escape('<name>'), '', str(i), flags=re.DOTALL)]
                else:
                    stikalo = 1
                    break
            if stikalo == 1:
                break
            for i in a:
                l += [re.sub('(.*?)'+re.escape('<artist>')+'(.*?)'+re.escape('<name>'), '', str(i), flags=re.DOTALL)]
            k = 0
            for i in l:
                if [re.sub(re.escape('</name>')+r'.*', '', str(i), flags=re.DOTALL)] not in h:
                    a = re.sub(re.escape('</name>')+r'.*', '', str(i), flags=re.DOTALL)
                    h += [a.replace('amp;', '')]
                self.seznam_albumov += [Album(izvajalec=str(h[k]), naslov=str(g[k]))]
                self.okno.l1.insert(k, self.seznam_albumov[k])
                k += 1

            stran += 1


class Album:
    def __init__(self, izvajalec='', naslov=''):
        self.izvajalec = izvajalec
        self.naslov = naslov
        self.pesmi = []
        self.trajanje = []
        self.gettags()

    def __repr__(self):
        return self.izvajalec + ' - ' + self.naslov

    def gettags(self):
        artist = self.izvajalec.replace(' ', '+').lower()
        album = self.naslov.replace(' ', '+').lower()
        url = ('http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key='
               + api
               + '&artist='
               + artist
               + '&album='
               + album
               + '&autocorrect=1')
        response = requests.get(url)
        soup = BeautifulSoup(response.content)

        _pesmi1 = soup('track')
        for p in _pesmi1:
            p1 = str(p('name')[0])
            p1 = re.sub('<name>', '', p1)
            p1 = re.sub('</name>', '', p1)
            p1.rstrip('}').lstrip('{')
            p2 = str(p('duration')[0])
            p2 = re.sub('<duration>', '', p2)
            p2 = re.sub('</duration>', '', p2)
            if not p2 == '':
                sek = str(int(p2) % 60)
                if len(sek) == 1:
                    sek = '0'+sek
                p2 = '{0}:{1}'.format(str(int(p2)//60), sek)
            self.pesmi += [p1]
            self.trajanje += [p2]

        _zanri = []
        _zanri1 = soup('tag')
        for t in _zanri1:
            t1 = str(t('name')[0])
            t1 = re.sub('<name>', '', t1)
            t1 = re.sub('</name>', '', t1)
            _zanri += [t1]
        zanri = ', '.join(_zanri)
        self.tags = zanri
